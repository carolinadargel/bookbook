import { combineReducers } from "redux";
import loginReducer from "./loginReducer";
import booksReducer from "./booksReducer";

export default combineReducers({ login: loginReducer, books: booksReducer })