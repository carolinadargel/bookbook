import { 
  ADD_BOOKS,
  ADD_READED,
  ADD_WISHLIST,
  ADD_WISHLIST_BOOK,
  ADD_READING_BOOK,
  ADD_READED_BOOK,
  ADD_READING,
  DELETE_BOOK_WISHLIST,
  DELETE_BOOK_READING,
  UPDATE_BOOK_REVIEW,
  CLEAR_BOOKS
} from "../actions/actionTypes";

const defaultBooks = {
  books: [],
  readed: [],
  wishlist: [],
  reading: [],
  search: "",
};

const timeline = (state = defaultBooks, action) => {
  const { book } = action;

  switch (action.type) {
    case ADD_BOOKS:
      const { books } = action;
      return {
        ...state,
        books: [...books],
      };
    case ADD_READED:
      const { readed } = action;
      return {
        ...state,
        readed: [...readed],
      };
    case ADD_WISHLIST:
      const { wishlist } = action;
      return {
        ...state,
        wishlist: [...wishlist],
      };

    case ADD_WISHLIST_BOOK:
      return {
        ...state,
        wishlist: [...state.wishlist, book],
      };

    case ADD_READING_BOOK:
      return {
        ...state,
        reading: [...state.reading, book],
      };

    case ADD_READED_BOOK:
      return {
        ...state,
        readed: [...state.readed, book],
      };

    case ADD_READING:
      const { reading } = action;
      return {
        ...state,
        reading: [...reading],
      };

    case UPDATE_BOOK_REVIEW:
      const { key, review, grade } = action;
      const newReaded = [...state.readed];
      newReaded[key].review = review;
      newReaded[key].grade = grade;
      return {
        ...state,
        readed: newReaded,
      };

    case DELETE_BOOK_WISHLIST:
      const newWishlist = [...state.wishlist];
      newWishlist.splice(action.key, 1);
      debugger;
      return {
        ...state,
        wishlist: newWishlist,
      };
      
    case DELETE_BOOK_READING:
      const newReading = [...state.reading];
      newReading.splice(action.key, 1);
      return {
        ...state,
        reading: newReading,
      };

    case CLEAR_BOOKS:
      return {
        ...state,
        books: [],
      };
    default:
      return state;
  }
};
export default timeline;
