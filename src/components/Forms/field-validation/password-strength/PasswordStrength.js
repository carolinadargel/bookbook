import React from "react";
import forceCalculator from "./force-calculator";

let dados = {
  width: "",
  height: "6px",
  marginTop: "0px",
  marginBottom: "0px",
  messageStrong: "",
  backgroundColor: "",
  transitionDuration: "0.4s",
};

class PasswordStrength extends React.Component {
  strongPasswordTests = () => {};

  componentWillReceiveProps() {
    let values = forceCalculator(this.props.valor);
    dados = { ...dados, ...values };
  }

  render() {
    return (
      <>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            width: "100%",
            height: "20px",
          }}
        >
          <div
            style={{
              width: "85%",
              height: "20px",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <div
              style={{
                width: "100%",
                height: "6px",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                backgroundColor: "#eeeeff",
              }}
            >
              <div style={dados}></div>
            </div>
          </div>

          <span>{dados.messageStrong}</span>
        </div>
        <span style={{ fontSize: 12.5 }}>
          A sua senha deve ter pelo menos 6 caracteres e conter pelo menos: uma
          letra maiúscula, uma letra minúscula e um dígito.
        </span>
      </>
    );
  }
}

export default PasswordStrength;
