const forceCalculator = (valor) => {
  let forca = 0;
  let cor = "";
  let msg = "";

  let shortPassword,
    longPassword,
    capitalLetters,
    smallLetters,
    numbers,
    metaCharacters = false;

  if (valor.length >= 6) {
    shortPassword = true;
  }
  if (valor.length >= 8) {
    longPassword = true;
  }
  if (valor.match(/[a-z]/)) {
    smallLetters = true;
  }
  if (valor.match(/[A-Z]/)) {
    capitalLetters = true;
  }
  if (valor.match(/[0-9]/)) {
    numbers = true;
  }
  if (valor.match(/\W+/)) {
    metaCharacters = true;
  }

  //Validação da força
  if (shortPassword) {
    forca += 10;
  }
  if (longPassword) {
    forca += 10;
  }
  if (capitalLetters) {
    forca += 10;
  }
  if (smallLetters) {
    forca += 10;
  }
  if (capitalLetters && smallLetters) {
    forca += 20;
  }
  if (numbers) {
    forca += 20;
  }
  if (metaCharacters) {
    forca += 20;
  }

  if (forca === 10 || forca === 20) {
    cor = "#F04323";
    msg = "Muito fraca";
  }
  if (forca === 30) {
    cor = "#F7842A";
    msg = "Fraca";
  }
  if (forca === 40 || forca === 50) {
    cor = "#FDD237";
    msg = "Média";
  }
  if (forca === 60 || forca === 70 || forca === 80) {
    cor = "#B7DD33";
    msg = "Forte";
  }
  if (forca === 80) {
    cor = "#8BE231";
    msg = "Forte";
  }
  if (forca === 90 || forca === 100) {
    cor = "#5BBA25";
    msg = "Muito Forte";
  }

  return { width: forca + "%", backgroundColor: cor, messageStrong: msg };
};

export default forceCalculator;
