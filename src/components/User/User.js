import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, notification } from "antd";
import { withRouter } from "react-router-dom";
const { Item } = Form;

class User extends React.Component {
  state = {
    name: "",
    user: "",
    email: "",
    password: "",
    button: true,
  };

  setField = (e, field) => {
    this.setState({ ...this.state, [field]: e.target.value });
  };

  getUser() {
    const userId = JSON.parse(localStorage.getItem("user")).id;
    const url = `https://ka-users-api.herokuapp.com/users/${userId}`;
    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("currentToken"),
      },
    })
      .then((res) => res.json())
      .then((data) =>
        this.setState({ name: data.name, user: data.user, email: data.email })
      );
  }

  componentDidMount() {
    this.getUser();
  }

  handleEdit = () => {
    this.setState({
      button: false,
    });
  };

  editUser = () => {
    const userId = JSON.parse(localStorage.getItem("user")).id;
    const url = `https://ka-users-api.herokuapp.com/users/${userId}`;
    const data = {
      user: {
        name: this.state.name,
        user: this.state.user,
        email: this.state.email,
        password: this.state.password,
        password_confirmation: this.state.password,
      },
    };
    fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("currentToken"),
      },
      body: JSON.stringify(data),
    }).then((res) => {
      if(res.ok){
        return notification.success(
          {
            message: "Cadastro atualizado",
          },
          3
        );
      }
    });
    this.setState({ button: true });
  };

  render() {
    return (
      <div className="FormPage">
        <Form
          layout="vertical"
          onFinish={this.onFinish}
          onFinishFailed={this.onFinishFailed}
          style={{ width: 700, padding: 70 }}
        >
          <Item label="NOME">
            <Input
              onChange={(e) => this.setField(e, "name")}
              value={this.state.name}
              placeholder="Nome"
              disabled={this.state.button}
              style={{ height: 50 }}
            />
          </Item>
          <Item label="USUARIO">
            <Input
              onChange={(e) => this.setField(e, "user")}
              value={this.state.user}
              placeholder="Usuario"
              disabled={this.state.button}
              style={{ height: 50}}
            />
          </Item>
          <Item label="EMAIL">
            <Input
              onChange={(e) => this.setField(e, "email")}
              value={this.state.email}
              placeholder="Email"
              disabled={this.state.button}
              style={{ height: 50 }}
            />
          </Item>
          <Item label="SENHA">
            <Input
              onChange={(e) => this.setField(e, "password")}
              value={this.state.password}
              placeholder="Password"
              disabled={this.state.button}
              style={{ height: 50 }}
            />
          </Item>
          {this.state.button ? (
            <Button
              type="primary"
              size="large"
              style={{ width: "100%", height: 64}}
              onClick={this.handleEdit}
            
            >
              Editar
            </Button>
          ) : (
            <Button
              type="primary"
              size="large"
              style={{ width: "100%", height: 64}}
              onClick={this.editUser}
              
            >
              Salvar
            </Button>
          )}
        </Form>
      </div>
    );
  }
}
export default withRouter(User);
