import React from "react";
import "antd/dist/antd.css";
import { withRouter, Link } from "react-router-dom";
import { Form, Input, Layout, Menu, Avatar, Button, notification } from "antd";
import { connect } from "react-redux";
import "./Visitor.css";
import { addBooks, clearBooks } from "../../actions";
import CardBooksVisitor from "../cardsBooks/CardsBooksVisitor";

const { Header } = Layout;
const { Item } = Menu;
const { Search } = Input;

class Timeline extends React.Component {
  state = {
    search: "",
  };

  handleSearchState = (e) => {
    this.setState({ search: e.target.value });
  };

  resetInput = (e) => {
    this.setState({
      search: "",
    });
  };

  handleClick = () => {
    const { booksDispatch } = this.props;
    const inputSearch = this.state.search;
    if (
      inputSearch.length <= 0 ||
      inputSearch === "" ||
      inputSearch === undefined
    ) {
      return notification.error(
        {
          message: "Ops, erro na pesquisa",
          description: "Preencha o campo de search",
        },
        4
      );
    } else {
      this.setState({ ...this.state, emptyInputError: false });
      const apiUrl = `https://www.googleapis.com/books/v1/volumes?q=${inputSearch}`;

      fetch(apiUrl)
        .then((res) => res.json())
        .then((res) => {
          booksDispatch(res.items);
          this.resetInput();
        });
    }
  };
  render() {
    const { books, clearBooksDispatch } = this.props;
    return (
      <>
        <Layout>
          <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
            <Menu
              theme="dark"
              mode="horizontal"
              defaultSelectedKeys={["1"]}
              style={{
                cursor: "pointer",
              }}
            >
              <Item key="1">
                <Link to="/login" onClick={clearBooksDispatch}>Fazer login</Link>
              </Item>
              <Item key="2">
                <Link to="/forms" onClick={clearBooksDispatch}>Cadastrar-se</Link>
              </Item>
              <Avatar
                style={{
                  cursor: "pointer",
                  float: "right",
                  marginTop: 12,
                  backgroundColor: "#5C73F2",
                }}
                size="large"
                title="Sair"
                ba
                onClick={() => {
                  window.localStorage.clear();
                  this.setState(
                    { authenticated: false },
                    this.props.history.push("/")
                  );
                  clearBooksDispatch();
                }}
              >
                {"Visitante"}
              </Avatar>
            </Menu>
          </Header>
        </Layout>
        <div className="ContainerVisitor">
          <Button
            type="primary"
            onClick={() => {
              this.props.history.push("/login");
            }}
          >
            Fazer Login
          </Button>
          <div className="StyleForm">
            <Form onFinish={this.handleClick}>
              <Search
                id="InputSearchVisitor"
                onSearch={this.handleClick}
                placeholder="search..."
                onChange={this.handleSearchState}
                enterButton
                compact
                size="large"
                style={{
                  width: "60vw",
                }}
                value={this.state.search}
              />
            </Form>
          </div>
          <CardBooksVisitor booksMap={books} />
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  books: state.books.books,
});

const mapDispatchToProps = (dispatch) => ({
  booksDispatch: (books) => dispatch(addBooks(books)),
  clearBooksDispatch: () => dispatch(clearBooks()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Timeline));
