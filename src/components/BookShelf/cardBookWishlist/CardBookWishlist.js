import React from "react";
import { connect } from "react-redux";
import { Card, Button } from "antd";
import { addReadingBook, deleteWishlist } from "../../../actions";

class CardBookWishlist extends React.Component {
  handleClick = (book, key) => {
    const idUser = JSON.parse(localStorage.getItem("user")).id;
    const idBook = book.id;
    const urlApi = `https://ka-users-api.herokuapp.com/users/${idUser}/books/${idBook}`;
    const data = {
      book: {
        shelf: 2,
      },
    };

    fetch(urlApi, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("currentToken"),
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((book) => {
        this.props.addReadingBook(book);
        this.props.deleteWishlist(key);
      });
  };

  render() {
    return (
      <>
        {this.props.wishlist.map((book, key) => {
          return (
            <Card
              loading={false}
              hoverable
              style={{ width: 441, height: 180, marginTop: 20 }}
              key={key}
            >
              <Card.Grid
                hoverable={false}
                style={{
                  width: 120,
                  height: 180,
                  padding: 0,
                  overflow: "hidden",
                }}
              >
                <img
                  style={{ height: 180 }}
                  alt="capa do livro"
                  src={book.image_url}
                />
              </Card.Grid>

              <Card.Grid
                hoverable={false}
                style={{
                  width: 320,
                  height: 180,
                  padding: 0,
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Card.Meta
                  style={{ width: "100%", margin: 0, padding: 14 }}
                  title={book.title}
                  description={book.review}
                />
                <Button
                  type="primary"
                  style={{
                    width: "91%",
                    height: 36,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    marginBottom: 14,
                  }}
                  onClick={() => this.handleClick(book, key)}
                >
                  Ler
                </Button>
              </Card.Grid>
            </Card>
          );
        })}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  wishlist: state.books.wishlist,
});

const mapDispatchToProps = (dispatch) => ({
  addReadingBook: (book) => dispatch(addReadingBook(book)),
  deleteWishlist: (key) => dispatch(deleteWishlist(key)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CardBookWishlist);
