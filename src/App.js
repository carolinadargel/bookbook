import React from "react";
import { withRouter, Route, Switch, Link } from "react-router-dom";
import { Result, Layout, Menu, Avatar, Button } from "antd";
import Forms from "./components/Forms/Forms";
import Login from "./components/Login/Login";
import Timeline from "./components/Timeline/Timeline";
import BookShelf from "./components/BookShelf/BookShelf";
import Visitor from "./components/Visitor/Visitor";
import User from "./components/User/User";
import { connect } from "react-redux";
import {clearBooks } from './actions';


const { Header } = Layout;
const { Item, SubMenu } = Menu;

class App extends React.Component {
  state = {
    authenticated: null,
    collapsed: false,
  };

  successLogin = () => {
    this.setState({ authenticated: true });
    this.props.history.push("/timeline");
  };

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  componentDidMount() {
    if (window.localStorage.getItem("currentToken") !== null) {
      this.setState((state) => {
        return { authenticated: true };
      });
    } else {
      this.setState({ authenticated: false });
    }
  }

  render() {
    const {clearBooksDispatch } = this.props;
    if (this.state.authenticated === null) return <div>Carregando...</div>;
    if (this.state.authenticated === false) {
      return (
        <>
          <Switch>
            <Route exact path="/">
              <Login login={this.successLogin} />
            </Route>
            <Route exact path="/forms">
              <Forms />
            </Route>
            <Route exact path="/login">
              <Login login={this.successLogin} />
            </Route>
            <Route exact path="/visitor">
              <Visitor />
            </Route>
            <Route path="/*">
              <Result
                status="403"
                title="403"
                subTitle="Desculpe, você não tem permissão para acessar esta página"
                extra={
                  <Button
                    type="primary"
                    onClick={() => {
                      this.props.history.push("/login");
                    }}
                  >
                    Voltar para Login
                  </Button>
                }
                style={{ marginTop: "150px" }}
              />
            </Route>
          </Switch>
        </>
      );
    }
    if (this.state.authenticated === true) {
      return (
        <>
          <Layout>
            <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
              <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={["1"]}
                style={{
                  cursor: "pointer",
                }}
                inlineCollapsed={this.state.collapsed}
              >
                <Item key="1"><Link to="/timeline" onClick={clearBooksDispatch}>Buscar Livros</Link></Item>
                <Item key="2"><Link to="/bookShelf">Estante</Link></Item>

                <SubMenu
                  style={{float: "right"}}
                  key="sub2"
                  icon={
                    <Avatar
                      style={{
                        cursor: "pointer",
                        float: "right",
                        marginTop: 12,
                        backgroundColor: "#5C73F2",
                      }}
                      size="large"
                      title="Minha conta"
                    >
                      {JSON.parse(
                        localStorage.getItem("user")
                      ).name[0].toUpperCase()}
                    </Avatar>
                  }
                >
                  <Menu.Item key="3">
                    <Link to="/user">Alterar Cadastro</Link>
                  </Menu.Item>

                  <Menu.Item onClick={() => {
                    window.localStorage.clear();
                    this.setState(
                      { authenticated: false },
                      this.props.history.push("/")
                    );
                  }}>
                    Sair
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </Header>
          </Layout>
          <Switch>
            <Route exact path="/timeline" key="1">
              <Timeline />
            </Route>
            <Route exact path="/bookShelf" key="2">
              <BookShelf />
            </Route>
            <Route exact path="/user" key="3">
              <User />
            </Route>
            <Route path="/*">
              <Result
                status="404"
                title="404"
                subTitle="Desculpe, esta página não existe"
                extra={
                  <Button
                    type="primary"
                    onClick={() => {
                      this.props.history.push("/timeline");
                    }}
                  >
                    Voltar para Timeline
                  </Button>
                }
                style={{ marginTop: "150px" }}
              />
            </Route>
          </Switch>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  books: state.books.books,
});

const mapDispatchToProps = (dispatch) => ({
  clearBooksDispatch: () => dispatch(clearBooks()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(App));